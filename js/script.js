var scrollY, distance = 40, speed = 1, animator;

function autoScrollTo(sectionId) {
    var currentY = window.pageYOffset;
    var targetY = document.getElementById(sectionId).offsetTop;
    var bodyHeight = document.body.offsetHeight;
    var yPos = currentY + window.innerHeight;

    var navHeight = document.getElementsByTagName('nav')[0].offsetHeight;

    animator = setTimeout('autoScrollTo("' + sectionId + '")', speed);

    if (yPos > bodyHeight) {
        clearTimeout(animator);
    } else {
        if (currentY < targetY - distance - navHeight) {
            scrollY = currentY + distance;
            window.scroll(0, scrollY);
        } else {
            clearTimeout(animator);
        }
    }
}

document.body.addEventListener("wheel", () => { clearTimeout(animator) });
// document.body.addEventListener("scroll", () => {
    
// });

window.onscroll = function() {
    var bodyPos = window.pageYOffset;
    console.log(document.querySelector('nav').style.color);
    document.querySelector('nav').style.backgroundColor = "rgba(255, 255, 255, " + (bodyPos / 100) + ") !important;";
};