<?php
    // use PHPMailer\PHPMailer\PHPMailer;
    // use PHPMailer\PHPMailer\Exception;

    // require 'phpmailer/src/Exception.php';
    // require 'phpmailer/src/PHPMailer.php';
    // require 'phpmailer/src/SMTP.php';

    // $mail = new PHPMailer();                              // Passing `true` enables exceptions
    // try {
    //     //Server settings
    //     $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    //     // $mail->isSMTP();                                      // Set mailer to use SMTP
    //     // $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    //     // $mail->SMTPAuth = true;                               // Enable SMTP authentication
    //     // $mail->Username = 'sabanusik@gmail.com';                 // SMTP username
    //     // $mail->Password = 'Zohar4272';                           // SMTP password
    //     // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //     // $mail->Port = 587;                                    // TCP port to connect to

    //     //Recipients
    //     $mail->setFrom('birenbergg@icloud.com', 'ליווי חשינוי');
    //     $mail->addAddress('birenbergg@icloud.com', '');     // Add a recipient
    //     // $mail->addAddress('ellen@example.com');               // Name is optional
    //     // $mail->addReplyTo('birenbergg@icloud.com', 'Information');
    //     // $mail->addCC('cc@example.com');
    //     // $mail->addBCC('bcc@example.com');

    //     //Attachments
    //     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //     //Content
    //     $mail->isHTML(true);                                  // Set email format to HTML
    //     $mail->Subject = 'Here is the subject from LOCAL';
    //     $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    //     // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    //     $mail->send();
    //     echo 'Message has been sent';
    // } catch (Exception $e) {
    //     echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    // }

    // the message
    $msg = "First line of text\nSecond line of text";

    // use wordwrap() if lines are longer than 70 characters
    $msg = wordwrap($msg,70);

    // send email
    mail("birenbergg@icloud.com","My subject",$msg);
?>

<!DOCTYPE html>
<html lang="he">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>ליווי לשינוי</title>
        
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>

    <link href="https://fonts.googleapis.com/css?family=Heebo" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/common.css" />
</head>

<body id="section-0">
    <header>
        <nav>
            <a href="#section-0"><img src="/images/logo.svg" alt="" style="max-height: 2em;"></a>
            <a href="#section-1">ליווי לשינוי</a>
            <a href="#section-2">אני צריך/ה ליווי</a>
            <a href="#section-3">אני המלווה שלך</a>
            <a href="#section-4">ליווי ולא טיפול</a>
            <a href="#section-5">יצירת קשר</a>
            <a href="#section-6">אנשים מספרים</a>
        </nav>

        <div id="header-image" style="display: flex; justify-content: center; align-items: center;">
            <div style="z-index: 1000;
        text-align: center;
        color: white;
        font-size: 200%;">
                <h1>ליווי לשינוי</h1>
                <p>כלים מעשיים לשינוי השיח הפנימי בליווי אישי</p>
            </div>
        </div>
    </header>

    <main>
        <section id="section-1">
            <h2>ליווי לשינוי</h2>
            <div class="section-body">
                <p>ליווי לשינוי היא הזדמנות לשותפות מעצימה, פתוחה, לא שיפוטית, ניטרלית בשיח הפנימי.</p>
                <p>מפגשים אישיים בגובה העיניים, בהם נפתח את השיח החוצה, נצא מהבדידות המחשבתית ולא נשאר איתו לבד, ננסה ביחד לנסח במילים מה שעד היום לא היה מנוסח בכל רם מכל מיני סיבות, נקבל שיקופים בהירים ובלתי תלויים מדמות יציבה ובלתי מעורבת בחיים, ממקום נקי ולא שופט. ננסה להבין איפה החסמים להתקדמות, מה מונע צמיחה, העמקה, דיוק עצמי, חיבור לרצונות פנימיים ללא הצורך לרצות, ללא הצורך לפעול מתוך לחצים וציפיות בלתי מציאותיות מצד המעגלים הקרובים והרחקים בחיינו.</p>
                <p>כמו כן נזכר בכוחות שיש ונלמד מהצלחות, נבין מה עובד, מה מצליח כרגע, מה עבד פעם, איזה דפוסים בהתמודדות היו יעילים לנו והאם יש מקום לגבש דפוסים יעילים יותר ומותאמים לכאן ועכשיו, נגבש ביחד אסטרטגיית התערבות, נכתוב תוכנית עבודה הוליסטית שתקיף כמה פרמטרים בתוך ההנחה שהכל קשור להכל אז נגע בכל הקשור לאיכות חיים, זוגיות, תעסוקה, לימודים, משפחה, חינוך, ניהול כלכלי. נתקדם בתוכנית צד אחר צד בקצב מותאם לכל אחד, אני מאמין שההשתנות תוך כדי הליווי היא תהליך שדורש התמדה ומחויבות הדדית. תהליך שבסופו יש תוצאות ברורות ומדידות.</p>
            </div>
        </section>

        <section id="section-2">
            <h2>אני צריך/ה ליווי</h2>
            <div class="section-body">
                <h3>שאל את עצמינו את השאלות הבאות כדי להחליט:</h3>

                <div class="q-container type-1">
                    <div class="q">האם הדמויות בחיים שלי הן יציבות?</div>
                    <div class="q">האם יש לי שותף בלתי תלוי בשיח הפנימי?</div>
                    <div class="q">האם יש לי מענה ויעוץ מקצועי ובלתי תלוי?</div>
                    <div class="q">האם אני מבין מה חוסם אותי מלהתקדם?</div>
                    <div class="q">האם יש לי אסטרטגיית התערבות ותוכנית עבודה?</div>
                    <div class="q">האם אני מתמיד בשינוי?</div>
                </div>
                    
                <p class="q-conclusion type-1">אם רוב התשובות הן שליליות אולי כדאי לקבוע פגישה.</p>

                <div class="q-container type-2">
                    <div class="q">האם אני תקוע כבר תקופה ולא מצליח להתקדם לבד או עם הכוחות הקיימים?</div>
                    <div class="q">האם אני רוצה לשנות משהו באורך החיים שלי ולא מצליח לבד?</div>
                    <div class="q">האם אני רוצה לשפר את איכות החיים שלי?</div>
                    <div class="q">האם אני רוצה ללמוד משהו חדש על עצמי?</div>
                    <div class="q">האם אני רוצה לדייק את עצמי?</div>
                    <div class="q">האם אני רוצה להיות הגרסה הכי טובה של עצמי?</div>
                </div>
                    
                <p class="q-conclusion type-2">אם רוב התשובות חיוביות אולי כדאי לקבוע פגישה.</p>
            </div>
        </section>

        <section id="section-3">
            <h2>אני המלווה שלך</h2>
            <div class="section-body">
                <p>הזהות המגדרית שלי הוא כלי, ,תשאל/י את עצמך מול מי שתלווה אותם בתהליך?</p>
                <p>הניסיון המקצועי שלי הם כלי, תשאל/י את עצמך האם האנשים סביבך יכולם לתת לך שיקוף מקצועי ובלתי תלוי על נוח לך יותר להפתח? מול דמות נשית או גברית חיובית ויציבה מצבך ולתת כלים להתפתחות? ברשותי תואר בפסיכולוגיה, לימודי CBT, השתלמויות מקצועיות.</p>
                <p>האישיות והניסיון שלי הם כלי, יש לי ניסיון של 6 שנים בליווי ילדים נוער ומשפחות, ברמה פרטנית וקבוצתית.</p>
                <p>עבדתי במגוון עמותות ותפקידים במגזר הצבורי, כגון: כתף לכתף, למרחב, עיריית כפר סבא, פ"ת, בת ים, משרד החינוך.</p>
                <p>אני לא מוותר לך ולא עליך, המחויבות שלי לתהליך המלווים שלי היא עצומה.</p>
            </div>
        </section>

        <section id="section-4">
            <h2>ליווי ולא טיפול</h2>
            <div class="section-body">
                <ul>
                    <li>בדרך כלל הטיפול מתוייג כמשהו שלילי, משהו שקשור בהפגת מחלה או בעיה נפשית, ליווי לשינוי לא נוגע בחולי אלא בבריאות ובשיפור איכות החיים.</li>
                    <li>הטיפול בדרך כלל תהליך ארוך יחסית, לפעמים אפילו של שנים, הליווי לשינוי הוא תהליך ממוקד וקצר יותר, תהליך המבוסס תוצאות.</li>
                    <li>הטיפול הקלאסי בדרך כלל מתעסק יותר בעבר, בליווי לשינוי יש התייחסות לעבר אבל הפוקס העיקרי הוא על ההווה ועל העתיד.</li>
                    <li>הטיפול בדרך כלל יקר יותר, ליווי לשינוי זול יותר לכן לא רק בעלי אמצעים יכולים להרשות ולהנות מהתהליך</li>
                    <li>הטיפול בדרך כלל סטרילי ופורמלי יותר, עם סטינג נוקשה יותר, ליווי לשינוי הוא יותר פתוח, בגובה העיניים, הדדי ושיוויוני יותר.</li>
                </ul>
            </div>
        </section>

        <section id="section-5">
            <h2>יצירת קשר</h2>
        </section>

        <section id="section-6">
            <h2>אנשים מספרים</h2>
        </section>
    </main>

    <footer>   
        <div id="copy">&copy; <?php echo date('Y') >= 2018 ? date('Y') : '2018&ndash;' . date('Y'); ?> ליווי לשינוי</div>
    </footer>

    <!-- <script src="js/script.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        // Select all links with hashes
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
                && 
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top - document.getElementsByTagName('nav')[0].offsetHeight
                    }, 300, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
    </script>
</body>
</html>